dep: ## fetch all dependencies
	go get -v -t ./...
	go get github.com/rakyll/gotest

tools: dep ## fetch useful tools for development
	go get -u golang.org/x/lint/golint
	go get github.com/codegangsta/gin
	go get github.com/smartystreets/goconvey

lint: tools ## lint the files
	${GOPATH}/bin/golint -set_exit_status ./...

build: dep ## build the binary
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v

test: dep ## run all tests
	${GOPATH}/bin/gotest ./... -v

test-cover: dep ## run tests with code coverage
	go test -cover ./...

test-race: dep ## run tests with race detector
	go test -race ./...

test-benchmark: dep ## run tests with benchmarks
	go test -bench=. ./...

run: dep ## directly run the code (installed golang necessary)
	go run $$(ls -1 *.go | grep -v _test.go)

run-goconvey: tools
	${GOPATH}/bin/goconvey -host 0.0.0.0 -port 8081
