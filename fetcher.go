package main

import (
    "context"
    "fmt"
    "github.com/docker/docker/api/types"
    "github.com/docker/docker/client"
    "log"
    "os"
    "strings"
)

func init() {
    if os.Getenv("DOCKER_API_VERSION") == "" {
        if err := os.Setenv("DOCKER_API_VERSION", "1.38"); err != nil {
            log.Fatalln(err)
        }
    }

    if os.Getenv("HOST_ENV_PARAM") == "" {
        if err := os.Setenv("HOST_ENV_PARAM", "VIRTUAL_HOST"); err != nil {
            log.Fatalln(err)
        }
    }
}

func processRunningContainers() error {
    cli, err := client.NewClientWithOpts(client.FromEnv)
    if err != nil {
        return fmt.Errorf("create new client: %s", err.Error())
    }
    defer cli.Close()

    containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
    if err != nil {
        return fmt.Errorf("fetch container list: %s", err.Error())
    }

    for _, container := range containers {
        if err := processRunningContainer(cli, container); err != nil {
            return err
        }
    }

    return nil
}

func processRunningContainer(cli *client.Client, container types.Container) error {
    containerData, err := cli.ContainerInspect(context.Background(), container.ID)
    if err != nil {
        return err
    }

    for _, env := range containerData.Config.Env {
        envSplit := strings.Split(env, "=")

        if len(envSplit) < 2 {
            continue
        }

        envKey := envSplit[0]
        envValue := envSplit[1]

        if envKey == os.Getenv("HOST_ENV_PARAM") {
            for _, hostName := range strings.Split(envValue, ",") {
                hl.add(newHost(hostName))
            }
        }
    }

    return nil
}
