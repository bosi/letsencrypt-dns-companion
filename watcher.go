package main

import (
    "context"
    "github.com/docker/docker/client"
    "github.com/radovskyb/watcher"
    "log"
    "os"
    "sync"
    "time"
)

const (
    certsDir     = "certs"
    certsLiveDir = "live"
    watchDir     = certsDir + "/" + certsLiveDir
)

var certFiles = map[string]string{
    "privkey.pem":   ".key",
    "fullchain.pem": ".crt",
    "chain.pem":     ".chain.pem",
}

type certWatcher struct {
    *watcher.Watcher
    event watcher.Event
}

var certW certWatcher
var isContainerRestarting = sync.Mutex{}

func (certW *certWatcher) init() {
    certW.Watcher = nil
    certW.Watcher = watcher.New()
    certW.Watcher.FilterOps(watcher.Create, watcher.Remove)
}

func (certW *certWatcher) run() {
    for {
        _, err := os.Stat(watchDir)
        if os.IsNotExist(err) {
            time.Sleep(10 * time.Second)
            continue
        }
        certW.init()
        certW.start()
    }
}

func (certW *certWatcher) start() {
    if err := certW.Watcher.Add(watchDir); err != nil {
        log.Println(err)
        return
    }
    go func() { certW.watch() }()
    log.Println("start watcher on: " + watchDir)
    if err := certW.Watcher.Start(time.Second * 10); err != nil {
        log.Println(err)
    }
    log.Println("watcher stopped")
}

func (certW *certWatcher) watch() {
    for {
        select {
        case certW.event = <-certW.Watcher.Event:
            certW.updateCerts()
            go func() { restartContainer() }()
        case err := <-certW.Watcher.Error:
            log.Println(err)
            certW.Watcher.Close()
        case <-certW.Watcher.Closed:
            return
        }
    }
}

func (certW *certWatcher) updateCerts() {
    if certW.event.IsDir() == false {
        return
    }
    if certW.event.Op == watcher.Create {
        certW.createCertsSymlinks()
    } else if certW.event.Op == watcher.Remove {
        certW.removeCertsSymlinks()
    }
}

func (certW *certWatcher) createCertsSymlinks() {
    certName := certW.event.Name()
    for file, suffix := range certFiles {
        fileName := certName + "/" + file
        if fileExist(watchDir + "/" + fileName) {
            log.Println("create symlink: " + certName + suffix + " -> " + file)
            err := os.Symlink(certsLiveDir+"/"+fileName, certsDir+"/"+certName+suffix)
            if err != nil {
                log.Println(err)
            }
        }
    }
}

func (certW *certWatcher) removeCertsSymlinks() {
    certName := certW.event.Name()
    for _, suffix := range certFiles {
        symlink := certsDir + "/" + certName + suffix
        log.Println("removing symlink: " + certName + suffix)
        _ = os.Remove(symlink)
    }
}

func restartContainer() {
    autostartContainer := os.Getenv("AUTOSTART_CONTAINER")
    if autostartContainer == "" {
        return
    }

    isContainerRestarting.Lock()
    defer isContainerRestarting.Unlock()

    cli, err := client.NewClientWithOpts(client.FromEnv)
    if err != nil {
        log.Println("create new client: " + err.Error())
        return
    }
    defer cli.Close()
    err = cli.ContainerRestart(context.Background(), autostartContainer, nil)
    if err != nil {
        log.Println(err)
    }
    log.Println("container " + autostartContainer + " was restarted")
}

func fileExist(file string) bool {
    fi, err := os.Stat(file)
    if os.IsNotExist(err) {
        return false
    }
    if fi.Mode().IsRegular() {
        return true
    }
    return false
}
