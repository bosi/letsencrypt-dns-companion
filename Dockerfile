############################
# STEP 1 build executable binary
############################
FROM golang:1.14 AS builder

WORKDIR $GOPATH/src/letsencrypt-dns-companion

RUN apt-get install git

COPY . .

RUN make build

############################
# STEP 2 build a small image
############################
FROM scratch

WORKDIR /app

COPY --from=builder /go/src/letsencrypt-dns-companion/letsencrypt-dns-companion /app/

ENTRYPOINT ["./letsencrypt-dns-companion"]
