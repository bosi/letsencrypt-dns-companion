package main

import (
    "log"
    "time"
)

func main() {
    go func() { certW.run() }()

    for {
        if err := processRunningContainers(); err != nil {
            log.Println(err)
        }

        if err := writeFile(); err != nil {
            log.Println(err)
        }
        time.Sleep(10 * time.Second)
        hl.removeExpired()
    }
}
