package main

import (
    "errors"
    "gopkg.in/yaml.v2"
    "io/ioutil"
    "log"
    "os"
    "reflect"
    "sort"
    "strconv"
    "strings"
    "time"
)

const (
    configDir      = "config"
    baseConfigFile = configDir + "/base-config.yml"
    configFile     = configDir + "/config.yml"
)

type conf struct {
    Draft bool `yaml:"draft"`
    Acme  struct {
        EmailAccount string `yaml:"email_account"`
        Staging      bool   `yaml:"staging"`
        APIVersion   int    `yaml:"api_version"`
        Crontab      string `yaml:"crontab_renew"`
    } `yaml:"acme"`
    Profiles     []profile     `yaml:"profiles"`
    Certificates []certificate `yaml:"certificates"`
}

type profile struct {
    Name     string `yaml:"name"`
    Provider string `yaml:"provider"`
    Options  struct {
        APIKey             string `yaml:"auth_api_key,omitempty"`
        APIPassword        string `yaml:"auth_api_password,omitempty"`
        ClientID           string `yaml:"auth_client_id,omitempty"`
        ClientIP           string `yaml:"auth_client_ip,omitempty"`
        ClientSecret       string `yaml:"auth_client_secret,omitempty"`
        CustomerID         string `yaml:"auth_customer_id,omitempty"`
        Endpoint           string `yaml:"endpoint,omitempty"`
        Filename           string `yaml:"filename,omitempty"`
        IbHost             string `yaml:"host,omitempty"`
        IbView             string `yaml:"ib_view,omitempty"`
        ID                 string `yaml:"auth_id,omitempty"`
        Key                string `yaml:"auth_key,omitempty"`
        KeyID              string `yaml:"auth_key_id,omitempty"`
        Password           string `yaml:"auth_password,omitempty"`
        Psw                string `yaml:"auth_psw,omitempty"`
        Port               string `yaml:"port,omitempty"`
        Region             string `yaml:"auth_region,omitempty"`
        ResourceGroup      string `yaml:"resource_group,omitempty"`
        Secret             string `yaml:"auth_secret,omitempty"`
        SecretKey          string `yaml:"auth_secret_key,omitempty"`
        Server             string `yaml:"auth_server,omitempty"`
        ServiceAccountInfo string `yaml:"auth_service_account_info,omitempty"`
        SubID              string `yaml:"auth_subid,omitempty"`
        SubUser            string `yaml:"auth_subuser,omitempty"`
        SubscriptionID     string `yaml:"auth_subscription_id,omitempty"`
        TenantID           string `yaml:"auth_tenant_id,omitempty"`
        Token              string `yaml:"auth_token,omitempty"`
        TwoFa              string `yaml:"auth_2fa,omitempty"`
        Username           string `yaml:"auth_username,omitempty"`
        Weight             string `yaml:"weight,omitempty"`
        ZoneID             string `yaml:"zone_id,omitempty"`
    } `yaml:"provider_options"`
    SleepTime int `yaml:"sleep_time"`
}

type certificate struct {
    Domains []string `yaml:"domains"`
    Profile string   `yaml:"profile"`
}

type host struct {
    hostname string
    expires  time.Time
}

type hostList struct {
    hosts []host
}

var hl hostList
var currHosts []host
var currConf conf

func init() {
    hl = hostList{}
    currConf = conf{}
}

func (hl *hostList) add(newHost host) {
    for i, h := range hl.hosts {
        if h.hostname == newHost.hostname {
            hl.hosts[i] = newHost
            return
        }
    }

    hl.hosts = append(hl.hosts, newHost)

    sort.SliceStable(hl.hosts, func(i, j int) bool {
        return hl.hosts[i].hostname < hl.hosts[j].hostname
    })
}

func (hl *hostList) removeExpired() {
    var newList []host

    for _, h := range hl.hosts {
        if h.expires.After(time.Now()) {
            newList = append(newList, h)
        }
    }

    hl.hosts = newList
}

func newHost(hostname string) host {
    ltEnv := os.Getenv("DOMAIN_LIFETIME_MINUTES")
    lt, err := strconv.Atoi(ltEnv)
    if err != nil {
        lt = 15
    }

    return host{
        hostname: strings.Trim(hostname, " "),
        expires:  time.Now().Add(time.Duration(lt) * time.Minute),
    }
}

func (hl *hostList) copy() []host {
    hosts := make([]host, len(hl.hosts))
    copy(hosts, hl.hosts)

    return hosts
}

func (hl *hostList) equal(hosts []host) bool {
    if len(hosts) != len(hl.hosts) {
        return false
    }

    for i, v := range hl.hosts {
        if v.hostname != hosts[i].hostname {
            return false
        }
    }
    return true
}

func writeFile() error {
    data, err := ioutil.ReadFile(baseConfigFile)
    if err != nil {
        return err
    }
    conf, err := toConf(data)
    if err != nil {
        return err
    }
    if len(conf.Profiles) == 0 {
        return errors.New("no profile was defined in" + baseConfigFile)
    }
    if conf.Profiles[0].Name == "" {
        return errors.New("no profile name was defined in " + baseConfigFile)
    }
    if reflect.DeepEqual(currConf, conf) && hl.equal(currHosts) {
        return nil
    }
    currConf, _ = toConf(data)
    for _, h := range hl.hosts {
        cert := certificate{
            Domains: []string{h.hostname},
            Profile: conf.Profiles[0].Name,
        }
        conf.Certificates = append(conf.Certificates, cert)
    }

    d, err := yaml.Marshal(&conf)
    if err != nil {
        return err
    }

    err = ioutil.WriteFile(configFile, d, 0600)
    if err != nil {
        return err
    }

    log.Println(configFile + " was written")
    currHosts = hl.copy()
    return nil
}

func toConf(slice []byte) (conf, error) {
    conf := conf{}
    err := yaml.Unmarshal(slice, &conf)
    conf.Certificates = nil
    return conf, err
}
