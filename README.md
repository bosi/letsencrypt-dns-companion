# Letsencrypt DNS Companion
This small app provides a helper for the docker image adferrand/dnsrobocert.

You need a base config file. An example can be found in config/base-config.yml.example

## Usage
Have a look at the `docker-compose.yml`, download it and run `docker-compose up` to start the application.

## Configuration
All parameters can be set via env:

| name | description | default |
|---|---|---|
|DOMAIN_LIFETIME_MINUTES| duration in minutes after which a domain will be seen as "dead" and will be removed from list|`15`|
|DOCKER_API_VERSION|version of the docker-api which is used for fetching container data|`1.38`|
|HOST_ENV_PARAM|name of the environment variable where the app should look for hostnames|`VIRTUAL_HOST`|
|AUTOSTART_CONTAINER|name of the container which will be added to list to let adferrand/letsencrypt-dns which container needs to be restarted after an update|[empty]|
